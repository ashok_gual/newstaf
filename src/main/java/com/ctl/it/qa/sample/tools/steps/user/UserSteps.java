package com.ctl.it.qa.sample.tools.steps.user;

import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.Step;

import java.util.List;

import com.ctl.it.qa.sample.tools.pages.demout.CompleteOrderPage;
import com.ctl.it.qa.sample.tools.pages.demout.RegistrationPage;
import com.ctl.it.qa.sample.tools.steps.CenturylinkSteps;
import com.ctl.it.qa.staf.Page;
import com.ctl.it.qa.staf.xml.reader.DataContainer;
import com.ctl.it.qa.staf.xml.reader.IntContainerField;
import com.ctl.it.qa.staf.xml.reader.IntDataContainer;


@SuppressWarnings("serial")
public class UserSteps extends CenturylinkSteps {
	
   /* RegistrationPage smallBusinessPage; 
    LoginPage loginPage;

    @Step
    public void is_in_centurylink_small_business_page() {
    	
    	 First line: envData is a variable to access Environment data in data input file (excel or xml). Here, url is accessed.
    	 Second line: Maximizing the browser
    	 Third line: shouldExist method checks the page has navigated to the correct page. It check by verifying the WebElementFacade mentioned in method getUniqueElementInPage() in HomePage class exists or not.
    	 
    	smallBusinessPage.openAt(envData.getFieldValue("url"));
    	smallBusinessPage.maximize();
    	shouldExist(smallBusinessPage, 30);
    }
    
    @Step
	public void is_in_sso_login_page(String url) {
		loginPage.openAt(url);
		loginPage.maximize();
		WaitForPageToLoad(20000);
		loginPage.btn_centurylink_sso.click();
	}
    
    @Step
	public void logs_in_sso_as(String userType) {
		loginPage.withTimeoutOf(Duration.ofMillis(6000)).waitFor(loginPage.btn_login);
		IntDataContainer dataContainer = envData.getContainer(
				loginPage.getClass().getSimpleName()).getContainer(userType);
		fillFields(loginPage, dataContainer.getMandatoryFieldsFromAllContainers());
		loginPage.btn_login.click();
	}

	*/
    RegistrationPage registrationPage;
    CompleteOrderPage completeOrderPage;
    @Step
    public void is_in_demout_portal_page() throws InterruptedException{
    	registrationPage.openAt(envData.getFieldValue("url"));
    	registrationPage.maximize();
    	shouldExist(registrationPage, 30);
    	Thread.sleep(5000);
    }
   /* @Step
    public void register_demout_page(){
    	/*IntDataContainer dataContainer = envData.getContainer(
    			registrationPage.getClass().getSimpleName()).getContainer(value);
		fillFields(registrationPage, dataContainer.getMandatoryFieldsFromAllContainers());
    	System.out.println(dataContainer);*/
    	
    	//  registrationPage.tbx_firstname.sendKeys(RegistrationPage.getFieldValue("tbx_firstname"));
    	
    		/*
    		IntDataContainer dataContainer = envData.getContainer("CommonData").getContainer("RegisterData");
    		registrationPage.tbx_firstname.sendKeys(dataContainer.getFieldValue("tbx_firstname"));
    		/*aaloginPage.tbx_password.sendKeys(dataContainer.getFieldValue("tbx_password"));
    		Thread.sleep(5000);
    		WebDriver proxiedDriver = getDriver() ;
    		JavascriptExecutor executor = (JavascriptExecutor)proxiedDriver;
    		executor.executeScript("arguments[0].click();", aaloginPage.loginCompButton);				
    		slf4jLogger.info("Sales executive logged into AA application successfully");
    		Thread.sleep(5000);*/
    

@Step
	public void register_demout_page() {
	System.out.println("outside cointainer");
		IntDataContainer dataContainer = envData.getContainer("CommonData").getContainer("RegisterData");
		System.out.println("inside cointainer");
		//registrationPage.tbx_firstname.sendKeys(dataContainer.getFieldValue("tbx_firstname"));
       fillAllFields(registrationPage, dataContainer);	
}
}